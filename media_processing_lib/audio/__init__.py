"""Init file"""
from .mpl_audio import MPLAudio
from .audio_reader import audio_read
from .audio_writer import audio_write
# Algorithms for MPLAudio
from .melspectrogram import melspectrogram
