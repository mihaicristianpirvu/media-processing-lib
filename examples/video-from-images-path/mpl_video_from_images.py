#!/usr/bin/env python3
"""
A script that simply takes a directory of images and output a video of them. Shape is infered from first image.
We can use --resolution to alter the final shape.
In:
- dir/
  - 0.png
  - 1.png
  - ...
  - N.png
Out:
- video.mp4
"""
from pathlib import Path
from natsort import natsorted
from media_processing_lib.video import MPLVideo
from media_processing_lib.video.backends import DiskBackend
from media_processing_lib.logger import mpl_logger as logger
from argparse import ArgumentParser

def get_args():
	parser = ArgumentParser()
	parser.add_argument("pngs_path", type=lambda p: Path(p).absolute())
	parser.add_argument("--out_file", "-o", type=lambda p: Path(p).absolute(), required=True)
	parser.add_argument("--resolution", type=int, nargs="+")
	parser.add_argument("--fps", type=int, default=30)
	parser.add_argument("--overwrite", action="store_true")
	args = parser.parse_args()
	assert args.pngs_path.exists(), f"'{args.pngs_path}' doesn't exist!"
	assert not args.out_file.exists() or args.overwrite, f"'{args.out_file}' exists. Use --overwrite"
	if args.resolution is not None:
		assert len(args.resolution) == 2
	return args

def main():
	args = get_args()
	file_paths = natsorted([x for x in args.pngs_path.glob("*.png")], key=lambda p: p.name)
	assert len(file_paths) > 0, f"No .png files were found. Only png files are allowed"
	data = DiskBackend(file_paths)
	video = MPLVideo(data, fps=args.fps)
	if args.resolution is not None:
		logger.info(f"Reshapging video to {args.resolution}")
		video = video.reshape(args.resolution)
	video.write(args.out_file)
	logger.info(f"Video stored at '{args.out_file}'")

if __name__ == "__main__":
	main()
