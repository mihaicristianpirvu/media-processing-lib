import numpy as np
import pandas as pd
from datetime import datetime
from media_processing_lib.video import MPLVideo, video_read

def main():
	savevid_libs = ["imageio", "opencv"]
	load_vid_libs = ["imageio", "pims", "opencv", "decord"]

	resolutions = [
		(10, 32, 32, 3),
		(100, 640, 480, 3)
	]

	for resolution in resolutions:
		print(f"Resolution: {resolution}")
		res = []
		data = np.random.randint(0, 255, size=resolution, dtype=np.uint8)
		video = MPLVideo(data, fps=10)
		video.save("/tmp/video.mp4")
		for savevid_lib in savevid_libs:
			for load_vid_lib in load_vid_libs:
				now = datetime.now()
				video = video_read("/tmp/video.mp4", video_lib=load_vid_lib)
				video.apply(lambda video, t : video[t][::-1])
				video.save("/tmp/tmp.mp4", video_lib=savevid_lib)
				took = datetime.now() - now
				res.append([savevid_lib, load_vid_lib, took.total_seconds()])
		df = pd.DataFrame(res, columns=["save_lib", "load_lib", "duration"]).sort_values("duration")
		print(df)
		print("=" * 50)
		df.to_csv(f"result_{resolution[1]}_{resolution[2]}.csv")

if __name__ == "__main__":
	main()
