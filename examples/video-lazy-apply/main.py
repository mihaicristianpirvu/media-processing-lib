import sys
import numpy as np
from tqdm import trange
from media_processing_lib.video import video_read, video_write

def main():
	video = video_read(sys.argv[1], video_lib="decord")
	print(f"Loaded video {sys.argv[1]}. FPS: {video.fps:.2f}. Shape: {video.shape}")
	print(video)

	# Manual way of applying a function to a video
	res_mirror = np.zeros(video.shape, dtype=np.uint8)
	for i in trange(len(video), desc="Manual apply"):
		frame = video[i]
		res_mirror[i] = frame[:, ::-1]

	# Using apply method of the MPL Object
	video_flip = video.apply(lambda frame, _, __: frame[::-1])
	print(video_flip)
	video_flip_mirror_1 = video.apply(lambda frame, _, __: frame[::-1, ::-1])
	print(video_flip_mirror_1)
	video_flip_mirror_2 = video_flip.apply(lambda frame, _, __: frame[:, ::-1])
	print(video_flip_mirror_2)

	# # Save manually the nun,mp array
	video.write("video_copy.mp4")
	video_write(res_mirror, "mirror.mp4", fps=video.fps)
	video_flip.save("flip.mp4", video_lib="opencv")
	video_flip_mirror_1.save("flip_mirror.mp4", video_lib="imageio")
	video_flip_mirror_2.save("flip_mirror_cv.mp4", video_lib="opencv")

if __name__ == "__main__":
	main()
