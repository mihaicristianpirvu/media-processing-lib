from pathlib import Path
import numpy as np
from media_processing_lib.video.backends.disk_backend import DiskBackend
from media_processing_lib.image import image_read
from media_processing_lib.utils import get_library_root

resources_dir = get_library_root() / "resources"
video_path = resources_dir / "test_video.mp4"
images_path = resources_dir / "images_path"
numpy_path = resources_dir / "numpy_path"


def test_disk_backend_constructor_1():
    paths = [x for x in images_path.iterdir()]
    DiskBackend(paths)

def test_disk_backend_constructor_2():
    try:
        DiskBackend(images_path)
    except:
        pass

def test_disk_backend_constructor_3():
    try:
        DiskBackend(video_path)
    except:
        pass

def test_disk_backend_constructor_3():
    try:
        DiskBackend([])
    except:
        pass

def test_disk_backend_constructor_4():
    paths = [x for x in images_path.iterdir()]
    _ = DiskBackend(paths, load_fn=image_read)

def test_disk_backend_constructor_5():
    paths = [x for x in images_path.iterdir()]
    _ = DiskBackend(paths, load_fn=lambda x: np.load(x)["arr_0"])

def test_disk_backend_len_1():
    # we know our test dir has 100 images
    paths = [x for x in images_path.iterdir()]
    res = DiskBackend(paths)
    assert len(res) == 100

def test_disk_backend_len_2():
    # we know our test dir has 100 images
    paths = [x for x in images_path.iterdir() if x.suffix==".png"]
    res = DiskBackend(paths)
    assert len(res) == 100

def test_disk_backend_len_3():
    # we know our test dir has 100 images
    paths = [x for x in numpy_path.iterdir() if x.suffix==".npz"]
    res = DiskBackend(paths)
    assert len(res) == 100

def test_disk_backend_getitem_1():
    # we know our test dir has 100 images
    paths = [x for x in images_path.iterdir() if x.suffix==".png"]
    res = DiskBackend(paths)
    item = res[10]
    assert item.shape == (480, 640, 3)

def test_disk_backend_getitem_2():
    # we know our test dir has 100 images
    paths = [x for x in numpy_path.iterdir() if x.suffix==".npz"]
    res = DiskBackend(paths)
    item = res[10]
    assert item.shape == (480, 640, 3)

def test_disk_backend_getitem_3():
    # we know our test dir has 100 images
    paths = [x for x in images_path.iterdir() if x.suffix==".png"]
    res = DiskBackend(paths)
    try:
        _ = res[100]
    except:
        pass

def test_disk_backend_getitem_4():
    paths = [x for x in images_path.iterdir() if x.suffix==".png"]
    res = DiskBackend(paths, load_fn=image_read)
    item = res[10]
    assert item.shape == (480, 640, 3)

def test_disk_backend_getitem_5():
    paths = [x for x in numpy_path.iterdir() if x.suffix==".npz"]
    res = DiskBackend(paths, load_fn=lambda x: np.load(x)["arr_0"])
    item = res[10]
    assert item.shape == (480, 640, 3)
