import numpy as np
from media_processing_lib.video import MPLVideo


def test_apply_1():
	data = np.random.randint(0, 255, size=(100, 32, 32, 3), dtype=np.uint8)
	video = MPLVideo(data, fps=30)
	new_video = video.apply(lambda item, v, t: item)
	assert new_video.shape == video.shape
	assert new_video.fps == video.fps
	for i in range(len(video)):
		assert np.allclose(new_video[i], video[i])
	assert new_video == video

def test_apply_2():
	def apply_fn(item, video, t):
		new_frame = video[t][::-1]
		return new_frame

	data = np.random.randint(0, 255, size=(100, 32, 32, 3), dtype=np.uint8)
	video = MPLVideo(data, fps=24)

	# simple apply
	new_video = video.apply(apply_fn)

	# apply manually on new data
	new_data = data.copy()
	for i in range(len(video)):
		new_data[i] = apply_fn(None, video, i)
	new_video_2 = MPLVideo(new_data, fps=video.fps)

	assert new_video.shape == new_video_2.shape
	assert new_video.fps == new_video_2.fps
	for i in range(len(new_video)):
		assert np.abs(new_video[i] - new_video_2[i]).sum() < 1e-5
	assert new_video == new_video_2

def test_apply_3():
	def apply_fn(item, video, t):
		new_frame = video[t][::-1]
		return new_frame

	data = np.random.randint(0, 255, size=(100, 32, 32, 3), dtype=np.uint8)
	video = MPLVideo(data, fps=24)

	# two applies of the same reversing => same video
	new_video = video.apply(apply_fn).apply(apply_fn)
	assert new_video == video


def main():
	test_apply_3()

if __name__ == "__main__":
	main()