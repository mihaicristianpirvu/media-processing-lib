import numpy as np
from media_processing_lib.video import MPLVideo


def test_constructor_1():
    data = np.random.randint(0, 255, size=(100, 32, 32, 3), dtype=np.uint8)
    video = MPLVideo(data, fps=30)
    assert not video is None

def test_fps_1():
    data = np.random.randint(0, 255, size=(100, 32, 32, 3), dtype=np.uint8)
    video = MPLVideo(data, fps=30)
    assert video.fps == 30

def test_fps_2():
    """FPS can be changed dynamically, since it only affects writing to disk."""
    data = np.random.randint(0, 255, size=(100, 32, 32, 3), dtype=np.uint8)
    video = MPLVideo(data, fps=30)
    video.fps = 100
    assert video.fps == 100

def test_frame_shape_1():
    """If frame shape is not provided, it is infered from first frame."""
    data = np.random.randint(0, 255, size=(100, 32, 32, 3), dtype=np.uint8)
    video = MPLVideo(data, fps=30)
    assert video.frame_shape == (32, 32)

def test_frame_shape_2():
    """Cannot set the frame shape, use .reshape() to create a new video."""
    data = np.random.randint(0, 255, size=(100, 32, 32, 3), dtype=np.uint8)
    video = MPLVideo(data, fps=30)
    try:
        video.frame_shape = (40, 40)
    except:
        pass

def test_frame_reshape_1():
    data = np.random.randint(0, 255, size=(100, 32, 32, 3), dtype=np.uint8)
    video = MPLVideo(data, fps=30).reshape((40, 40))
    assert video.frame_shape == (40, 40)
    assert video[0].shape == (40, 40, 3)

def test_frame_shape_4():
    """Frame shape must be a HxW tuple only"""
    data = np.random.randint(0, 255, size=(100, 32, 32, 3), dtype=np.uint8)
    try:
        _ = MPLVideo(data, fps=30, frame_shape=(40, 40, 3))
    except:
        pass

def test_shape_1():
    data = np.random.randint(0, 255, size=(100, 32, 32, 3), dtype=np.uint8)
    video = MPLVideo(data, fps=30)
    assert video.shape == (100, 32, 32, 3)

def test_shape_2():
    data = np.random.randint(0, 255, size=(100, 32, 32, 3), dtype=np.uint8)
    video = MPLVideo(data, fps=30).reshape((50, 16))
    assert video.shape == (100, 50, 16, 3)

def test_len_1():
    data = np.random.randint(0, 255, size=(100, 32, 32, 3), dtype=np.uint8)
    video = MPLVideo(data, fps=30)
    assert len(video) == 100

def test_slice_1():
    data = np.random.randint(0, 255, size=(100, 32, 32, 3), dtype=np.uint8)
    video = MPLVideo(data, fps=30)
    video2 = video[50:70]
    assert len(video2) == 20
    assert np.allclose(video[55], video2[5])

def main():
    # TestMPLVideo().test_apply_1()
    pass


if __name__ == "__main__":
    main()
