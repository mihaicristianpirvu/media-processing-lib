import numpy as np
from copy import copy
from media_processing_lib.audio import MPLAudio, audio_read

def test_constructor_1():
	data = np.random.randn(100000).astype(np.float32)
	audio = MPLAudio(data, sample_rate=16000)
	assert not audio is None

def test_copy_1():
	data = np.random.randn(100000).astype(np.float32)
	audio = MPLAudio(data, sample_rate=16000)
	audio2 = copy(audio)
	assert np.abs(audio.data - audio2.data).sum() < 1e-5

def test_resample_1():
	data = np.ones((100000, )).astype(np.float32)
	audio = MPLAudio(data, sample_rate=10000)
	for newSr in [500, 1000, 5000, 20000]:
		assert (audio.resample(newSr).data.mean() - 1) < 1e-5

def test_write_1():
	data = np.random.randn(100000).astype(np.float32)
	audio = MPLAudio(data, sample_rate=16000)
	audio.save("/tmp/tmpwav.wav", audio_lib="soundfile")
	audio2 = audio_read("/tmp/tmpwav.wav", audio_lib="librosa", sr=16000)
	assert audio == audio2

	# def test_apply_2():
	# 	def applyFn(video, t):
	# 		newFrame = video[t][::-1]
	# 		return newFrame

	# 	data = np.random.randint(0, 255, size=(100, 32, 32, 3), dtype=np.uint8)
	# 	video = MPLVideo(data, fps=24)

	# 	newVideo = video.apply(applyFn)

	# 	newData = data.copy()
	# 	for i in range(len(video)):
	# 		newData[i] = applyFn(video, i)
	# 	newVideo2 = MPLVideo(newData, fps=video.fps)

	# 	assert newVideo.shape == newVideo2.shape
	# 	assert newVideo.fps == newVideo2.fps
	# 	assert np.abs(newVideo.data - newVideo2.data).sum() < 1e-5
