import numpy as np
from media_processing_lib.audio import audio_read, audio_write

class TestAudioIO:
	def test_read_1(self):
		try:
			audio_read("/path/to/nowhere")
			assert False
		except Exception as e:
			pass

	def test_write_1(self):
		data = np.random.randn(100000).astype(np.float32)
		audio_write(data, "/tmp/tmpwav.wav", sample_rate=16000)

	# def test_write_1(self):
	# 	data = np.random.randint(0, 255, size=(100, 32, 32, 3), dtype=np.uint8)
	# 	video = MPLVideo(data, fps=30)
	# 	tryWriteVideo(video, "/tmp/video.mp4")

	# def test_read_write_1(self):
	# 	data = np.random.randint(0, 255, size=(100, 32, 32, 3), dtype=np.uint8)
	# 	video = MPLVideo(data, fps=30)

	# 	saveVidLibs = ["imageio", "opencv"]
	# 	loadVidLibs = ["imageio", "pims", "opencv"]

	# 	for saveVidLib in saveVidLibs:
	# 		video.save("/tmp/video_%s.mp4" % saveVidLib, vidLib=saveVidLib)

	# 	readVideos = {}
	# 	for saveVidLib in saveVidLibs:
	# 		readVideos[saveVidLib] = {}
	# 		for loadVidLib in loadVidLibs:
	# 			readVideo = tryReadVideo("/tmp/video_%s.mp4" % saveVidLib, vidLib=loadVidLib)
	# 			readVideos[saveVidLib][loadVidLib] = readVideo
	# 			assert len(video) == len(readVideo)
	# 			assert video.fps == readVideo.fps
	# 			# Data is not guaranteed to be equal with videos... Lossy compression :)

	# 	for saveVidLib in saveVidLibs:
	# 		firstKey = tuple(readVideos[saveVidLib])[0]
	# 		firstRead = readVideos[saveVidLib][firstKey]
	# 		for loadVidLib in loadVidLibs:
	# 			for i in range(len(video)):
	# 				frame1 = firstRead[i]
	# 				frame2 = readVideos[saveVidLib][loadVidLib][i]
	# 				assert np.abs(frame1 - frame2).sum() < 1e-5