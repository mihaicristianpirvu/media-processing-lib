import numpy as np

from media_processing_lib.image import image_write
from media_processing_lib.image.libs_builder import get_available_image_libs

def test_write_1():
    image = np.random.randint(0, 255, size=(30, 30, 3))
    for img_lib in get_available_image_libs():
        image_write(image, f"/tmp/image_{img_lib}.png", img_lib=img_lib)
