import numpy as np
from media_processing_lib.image import image_add_text

def test_image_add_text_1():
    zeros = np.zeros((200, 200, 3), dtype=np.uint8)
    b1 = image_add_text(zeros, "Hello", (0, 0))
    # Add Hel, then, on top of it, Hello
    b2 = image_add_text(image_add_text(zeros, "Hel", (0, 0)), "Hello", (0, 0))
    b3 = image_add_text(zeros, "Hello", (0, 0))
    # Add Jel, then, on top of it, Hello
    b4 = image_add_text(image_add_text(zeros, "Jel", (0, 0)), "Hello", (0, 0))
    d = image_add_text(zeros, "Yellow", (0, 0))
    assert (b1 != d).sum() > 0
    # b1 > 0 binaries it. J = (b1 > 0) + (b2 > 0) sums two binary maps, then we binary it again
    # b3 > 0 binaries it. K =(J > 0) != (b3 > 0) finds where the 3 binary maps would differ
    # K != zeros would mean that (b1 + b2 - b3) in binary form would not cancel each other out.
    assert (((((b1 > 0) + (b2 > 0)) > 0) != (b3 > 0)) != zeros).sum() == 0
    assert (b1 != b4).sum() > 0
