from media_processing_lib.collage_maker import collage_fn
import numpy as np

def test_collage_fn_1():
    images = np.random.randint(0, 255, size=(3, 420, 420, 3), dtype=np.uint8)
    collage = collage_fn(images)
    assert collage.shape == (840, 840, 3)

def test_collage_fn_pad_1():
    images = np.random.randint(0, 255, size=(3, 420, 420, 3), dtype=np.uint8)
    collage = collage_fn(images, pad_bottom=20, pad_right=30, rows_cols=(2, 2))
    assert collage.shape == (860, 870, 3)

def test_collage_fn_pad_2():
    images = np.random.randint(0, 255, size=(3, 420, 420, 3), dtype=np.uint8)
    collage = collage_fn(images, pad_bottom=20, pad_right=30, rows_cols=(3, 1))
    assert collage.shape == (1300, 420, 3)

def test_collage_fn_pad_3():
    images = np.random.randint(0, 255, size=(3, 420, 420, 3), dtype=np.uint8)
    collage = collage_fn(images, pad_bottom=20, pad_right=30, rows_cols=(1, 3))
    assert collage.shape == (420, 1320, 3)

def test_collage_fn_none_images():
    images = [np.random.randint(0, 255, size=(420, 420, 3), dtype=np.uint8) for _ in range(3)]
    images = [images[0], None, images[1], None, images[2]]
    collage = collage_fn(images)
    assert collage.shape == (840, 1260, 3)

def test_collage_fn_pad_to_max():
    desired_shape = (420, 420, 3)
    images = [np.random.randint(0, 255, size=desired_shape, dtype=np.uint8)]
    for _ in range(8):
        shape = (desired_shape[0] - np.random.randint(10), desired_shape[1] - np.random.randint(10), 3)
        new_img = np.random.randint(0, 255, size=shape, dtype=np.uint8)
        images.append(new_img)

    try:
        _ = collage_fn(images)
        raise Exception
    except ValueError:
        pass

    collage = collage_fn(images, pad_to_max=True, rows_cols=(3, 3))
    assert collage.shape == (420 * 3, 420 * 3, 3)
