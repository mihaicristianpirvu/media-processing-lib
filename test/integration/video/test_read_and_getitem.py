from typing import Dict
import numpy as np
from media_processing_lib.video.backends import MPLVideoBackend
from media_processing_lib.video.utils import get_available_video_read_libs, build_video_read_fn
from media_processing_lib.utils import get_library_root

video_path = get_library_root() / "resources" / "test_video.mp4"


def test_read_and_getitem_mp4():
    backends: Dict[str, MPLVideoBackend] = {k: build_video_read_fn(k) for k in get_available_video_read_libs()}
    videos = {}
    for k, backend in backends.items():
        try:
            # this skips disk/memory backends
            videos[k] = backend(video_path)
        except:
            pass
    assert len(videos) > 1, f"At least one video required"
    print(f"Available libs: {list(videos.keys())}")
    n_frames = len(videos[list(videos.keys())[0]])
    assert len(videos) > 0 and n_frames > 0
    random_ixs = np.random.permutation(n_frames)[0:10]
    for ix in random_ixs:
        frames = [video[int(ix)] for video in videos.values()]
        first, rest = frames[0], frames[1:]
        X = [np.allclose(first, other) for other in rest]
        assert sum(X) == len(rest)

if __name__ == "__main__":
    test_read_and_getitem_mp4()
